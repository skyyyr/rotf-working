SpawnArea = {
	promptBody = {
		"Move to the location where you'd like the spawn egg to be and type \"here\" in the box to set its spawn point.",
		"Enter the spawn template name of the NPC (e.g. stormtrooper, nightsister_elder)",
		"Enter the number of NPCs to spawn.",
		"Enter the level of the event NPC's to spawn.",
		"Enter the radius of the spawn egg area or Enter \"patrol\" to create a patrol spawn.",
		"Enter the spawn template name of the NPC (e.g. stormtrooper, nightsister_elder)",
		"Enter the number of NPCs to spawn.",
		"Enter the level of the event NPC's to spawn.",
		"Enter the radius of the spawn egg area."
	},

	promptTitle = { "Spawn Location", "NPC Template", "Number of NPCs", "NPC Level", "Spawn Radius or Patrol", "NPC Template", "Number of NPCs", "NPC Level", "Spawn Radius" },

	spawnAreaCleanup = 8 * 60 * 60, -- Automatic cleanup after 8 hours

}

function SpawnArea:resetDefaults(pPlayer)
	local playerID = SceneObject(pPlayer):getObjectID()

	deleteData(playerID .. ":SpawnArea:setupStep")
	deleteData(playerID .. ":SpawnArea:setupCompleted")
	deleteData(playerID .. ":SpawnArea:radius")
	deleteData(playerID .. ":SpawnArea:numSpawns")
	deleteData(playerID .. ":SpawnArea:spawnPointX")
	deleteData(playerID .. ":SpawnArea:spawnPointZ")
	deleteData(playerID .. ":SpawnArea:spawnPointY")
	deleteData(playerID .. ":SpawnArea:spawnLevel")
	deleteData(playerID .. ":SpawnArea:amountOfPatrol")
	deleteData(playerID .. ":SpawnArea:patrolPoint")
	deleteData(playerID .. ":spawnArea:amountOfCurrent")

	deleteStringData(playerID .. ":SpawnArea:spawnTemplate")
	deleteStringData(playerID .. ":SpawnArea:spawnPlanet")
end

function SpawnArea:spawnAreaEgg(pPlayer)
	local playerID = SceneObject(pPlayer):getObjectID()

	local posX = readData(playerID .. ":SpawnArea:spawnPointX")
	local posZ = readData(playerID .. ":SpawnArea:spawnPointZ")
	local posY = readData(playerID .. ":SpawnArea:spawnPointY")
	local planet = readStringData(playerID .. ":SpawnArea:spawnPlanet")
	local setup = readData(playerID .. ":SpawnArea:setupStep")

	local pSpawnEgg = spawnSceneObject(planet, "object/tangible/beta/beta_terminal_food.iff", posX, posZ, posY, 0, 0)
	
	if (pSpawnEgg == nil) then
		CreatureObject(pPlayer):sendSystemMessage("Error creating spawn egg. Please try again.")
		return nil
	end
	
	local spawnEggID = SceneObject(pSpawnEgg):getObjectID()
    
	CreatureObject(pSpawnEgg):setCustomObjectName("Spawn Egg")

	writeData(playerID .. ":SpawnArea:spawnEggID", SceneObject(pSpawnEgg):getObjectID())
	
	local playerID = SceneObject(pPlayer):getObjectID()
	local template = readStringData(playerID .. ":SpawnArea:spawnTemplate")
	local mobLevel = readData(playerID .. ":SpawnArea:spawnLevel")
	local numSpawns = readData(playerID .. ":SpawnArea:numSpawns")
    local radius = readData(playerID .. ":SpawnArea:radius")
	local heading = readData(playerID .. ":SpawnArea:heading")
	
	writeStringData(spawnEggID .. ":SpawnArea:spawnTemplate", template)
	writeData(spawnEggID .. ":SpawnArea:spawnLevel", mobLevel)
	writeData(spawnEggID .. ":SpawnArea:numSpawns", numSpawns)
	writeData(spawnEggID .. ":SpawnArea:radius", radius)
	writeData(spawnEggID .. ":SpawnArea:heading", heading)
	writeStringData(spawnEggID .. ":SpawnArea:spawnPlanet", planet)
	writeData(spawnEggID .. ":SpawnArea:spawnPointX", posX)
	writeData(spawnEggID .. ":SpawnArea:spawnPointY", posY)
	writeData(spawnEggID .. ":SpawnArea:spawnPointZ", posZ)

	createEvent(self.spawnAreaCleanup * 1000, "SpawnArea", "cleanUp", pPlayer, "")
    --createEvent(1000, "SpawnArea", "spawnNpcs", pPlayer, "")
    
	return pSpawnEgg
end

function SpawnArea:spawnNpcs(pPlayer)
	if (pPlayer == nil) then
		return
	end

	local playerID = SceneObject(pPlayer):getObjectID()
	local posX = readData(playerID .. ":SpawnArea:spawnPointX")
	local posZ = readData(playerID .. ":SpawnArea:spawnPointZ")
	local posY = readData(playerID .. ":SpawnArea:spawnPointY")
	local template = readStringData(playerID .. ":SpawnArea:spawnTemplate")
	local mobLevel = readData(playerID .. ":SpawnArea:spawnLevel")
	local numSpawns = readData(playerID .. ":SpawnArea:numSpawns")
    local radius = readData(playerID .. ":SpawnArea:radius")
	local heading = readData(playerID .. ":SpawnArea:heading")
	local planet = readStringData(playerID .. ":SpawnArea:spawnPlanet")

    for i = 1, numSpawns, 1 do
	    local modX = getRandomNumber(0,10)
		local modY = getRandomNumber(0,10)
		if (modX > 5) then
			modX = -1
		else
			modX = 1
		end
		
		if (modY > 5) then
			modY = -1
		else
			modY = 1
		end
	    local xOffset = getRandomNumber(0, radius) * modX
		local yOffset = getRandomNumber(0, radius) * modY
        local pMobile = spawnEventMobile(planet, template, mobLevel, posX + xOffset, posZ, posY + yOffset, heading, 0)
	end
end

function SpawnArea:spawnNpcsByMenu(pSpawnEgg)
    if (pSpawnEgg == nil) then
        return
    end
    
    local spawnEggID = SceneObject(pSpawnEgg):getObjectID()
    local posX = readData(spawnEggID .. ":SpawnArea:spawnPointX")
    local posZ = readData(spawnEggID .. ":SpawnArea:spawnPointZ")
    local posY = readData(spawnEggID .. ":SpawnArea:spawnPointY")
    local template = readStringData(spawnEggID .. ":SpawnArea:spawnTemplate")
    local mobLevel = readData(spawnEggID .. ":SpawnArea:spawnLevel")
    local numSpawns = readData(spawnEggID .. ":SpawnArea:numSpawns")
    local radius = readData(spawnEggID .. ":SpawnArea:radius")
    local heading = readData(spawnEggID .. ":SpawnArea:heading")
    local planet = readStringData(spawnEggID .. ":SpawnArea:spawnPlanet")

    for i = 1, numSpawns, 1 do
        local modX = getRandomNumber(0,10)
        local modY = getRandomNumber(0,10)
        if (modX > 5) then
            modX = -1
        else
            modX = 1
        end
        
        if (modY > 5) then
            modY = -1
        else
            modY = 1
        end
        local xOffset = getRandomNumber(0, radius) * modX
        local yOffset = getRandomNumber(0, radius) * modY
        local pMobile = spawnEventMobile(planet, template, mobLevel, posX + xOffset, posZ, posY + yOffset, heading, 0)
        if (pMobile == nil) then
            return nil
        end
        local mobileID = SceneObject(pMobile):getObjectID()
        createObserver(OBJECTDESTRUCTION, "SpawnArea", "notifyMobileDied", pMobile)
        writeData(SceneObject(pMobile):getObjectID() .. ":SpawnArea:mobileEgg", spawnEggID)
    end
end

function SpawnArea:notifyMobileDied(pVictim)
	if (pVictim == nil) then
		return 0
	end

	local victimID = SceneObject(pVictim):getObjectID()
	local spawnEggID = readData(victimID .. ":SpawnArea:mobileEgg")
	local eggNumSpawns = readData(spawnEggID .. ":SpawnArea:numSpawns")
	local pSpawnEgg = getSceneObject(spawnEggID)
	local modified = readData(spawnEggID .. ":SpawnArea:modifyOrRemove")

	writeData(spawnEggID .. ":SpawnArea:mobileKilled", readData(spawnEggID .. ":SpawnArea:mobileKilled") + 1)
	local killedSpawn = readData(spawnEggID .. ":SpawnArea:mobileKilled")
	
	if (eggNumSpawns <= killedSpawn and modified == 0) then
		createEvent(1000, "SpawnArea", "spawnNpcsByMenu", pSpawnEgg, "")
		writeData(spawnEggID .. ":SpawnArea:mobileKilled", 0)
	end
	return 1
end

function SpawnArea:cleanUp(pPlayer)
	if (pPlayer == nil) then
		return
	end

	self:resetDefaults(pPlayer)
	self:destroySpawnEgg(pPlayer)
end

function SpawnArea:destroySpawnEgg(pPlayer)
	local playerID = SceneObject(pPlayer):getObjectID()

	local spawnEggID = readData(playerID .. ":SpawnArea:spawnEggID")
	local pSpawnEgg = getSceneObject(spawnEggID)

	if (pSpawnEgg ~= nil) then
		SceneObject(pSpawnEgg):destroyObjectFromWorld()
	end

	deleteData(playerID .. ":SpawnArea:spawnEggID")
end

function SpawnArea:suiSpawnEggMainCallback(pPlayer, pSui, eventIndex, args)
	local cancelPressed = (eventIndex == 1)

	if (cancelPressed) then
		return
	end

	local playerID = SceneObject(pPlayer):getObjectID()
	local spawnEggID = readData(playerID .. ":SpawnArea:spawnEggID")
    local level = readData(playerID .. ":SpawnArea:spawnLevel")
	local pSpawnEgg = getSceneObject(spawnEggID)

	if (args == "setup") then
		self:resetDefaults(pPlayer)
		writeData(playerID .. ":SpawnArea:setupStep", 1)
		self:showSetupUI(pPlayer)
		return
	elseif (args == "data") then
		local numSpawns = readData(playerID .. ":SpawnArea:numSpawns")

		if (numSpawns == 0) then
			CreatureObject(pPlayer):sendSystemMessage("Spawn Egg: Number of spawns: " .. numSpawns)
		else
			local sTemp = readStringData(playerID .. ":SpawnArea:spawnTemplate")
			CreatureObject(pPlayer):sendSystemMessage("Spawn Egg: Number of spawns: " .. numSpawns .. ". Spawn template: " .. sTemp .. " . Spawn Level: " .. level)
		end
	elseif (args == "quit") then
		self:resetDefaults(pPlayer)
		self:destroySpawnEgg(pPlayer)
		return
    elseif (args == "s") then
		self:spawnNpcs(pPlayer)
		local sui = SuiInputBox.new("SpawnArea", "suiSpawnEggMainCallback")
		sui.setTargetNetworkId(SceneObject(pPlayer):getObjectID())
		local suiBody = "Enter \"setup\" to setup data, \"data\" to view current data, \"quit\" to end. Enter \"s\" to spawn another wave."
		sui.setTitle("Information")
		sui.setPrompt(suiBody)
		sui.sendTo(pPlayer)
		return
	end

	self:showMainUI(pPlayer)
end

function SpawnArea:showMainUI(pPlayer)
	local sui = SuiInputBox.new("SpawnArea", "suiSpawnEggMainCallback")

	sui.setTargetNetworkId(SceneObject(pPlayer):getObjectID())

	local suiBody = "Enter \"setup\" to setup data, \"data\" to view current data, \"quit\" to end. Enter \"s\" to spawn another wave."
	sui.setTitle("Information")
	sui.setPrompt(suiBody)

	sui.sendTo(pPlayer)
end

function SpawnArea:showSetupUI(pPlayer)
	local playerID = SceneObject(pPlayer):getObjectID()
	local curStep = readData(playerID .. ":SpawnArea:setupStep")

	if (readData(playerID .. ":SpawnArea:setupCompleted") == 1 or curStep == 0) then
		self:showMainUI(pPlayer)
		return
	end

	local sui = SuiInputBox.new("SpawnArea", "suiSpawnEggSetupCallback")

	sui.setTargetNetworkId(SceneObject(pPlayer):getObjectID())

	sui.setTitle(self.promptTitle[curStep])
	sui.setPrompt(self.promptBody[curStep])

	sui.sendTo(pPlayer)
end

function SpawnArea:suiSpawnEggSetupCallback(pPlayer, pSui, eventIndex, args)
	local cancelPressed = (eventIndex == 1)

	if (cancelPressed) then
		return
	end

	local playerID = SceneObject(pPlayer):getObjectID()
	local curStep = readData(playerID .. ":SpawnArea:setupStep")

	if (curStep == 1) then
		if (args == "here") then
			if (SceneObject(pPlayer):getParentID() ~= 0) then
				CreatureObject(pPlayer):sendSystemMessage("ERROR: You must be outside to set a spawn area.")
			else
				writeData(playerID .. ":SpawnArea:spawnPointX", math.floor(SceneObject(pPlayer):getWorldPositionX()))
				writeData(playerID .. ":SpawnArea:spawnPointZ", math.floor(SceneObject(pPlayer):getWorldPositionZ()))
				writeData(playerID .. ":SpawnArea:spawnPointY", math.floor(SceneObject(pPlayer):getWorldPositionY()))
				writeStringData(playerID .. ":SpawnArea:spawnPlanet", SceneObject(pPlayer):getZoneName())
				writeData(playerID .. ":SpawnArea:setupStep", 2)
				CreatureObject(pPlayer):sendSystemMessage("Spawn Area: Set to your current location.")
			end
		else
			CreatureObject(pPlayer):sendSystemMessage("You must stand where you want the spawn egg and type \"here\" in the input box.")
		end
	elseif (curStep == 2) then
		if (args == "") then
			CreatureObject(pPlayer):sendSystemMessage("You must enter a spawn template.")
		else
			writeStringData(playerID .. ":SpawnArea:spawnTemplate", args)
			CreatureObject(pPlayer):sendSystemMessage("NPC Template: " .. args)
			writeData(playerID .. ":SpawnArea:setupStep", 3)
		end
	elseif (curStep == 3) then
		local spawnNumber = tonumber(args)

		if (spawnNumber ~= nil and spawnNumber > 0 and spawnNumber <= 20) then
			writeData(playerID .. ":SpawnArea:numSpawns", spawnNumber)
			writeData(playerID .. ":SpawnArea:setupStep", 4)
		else
			CreatureObject(pPlayer):sendSystemMessage("You must enter a spawn number between 1 and 20.")
		end
	elseif (curStep == 4) then
		local spawnLevel = tonumber(args)

		if (spawnLevel ~= nil and spawnLevel > 0) then
			writeData(playerID .. ":SpawnArea:spawnLevel", spawnLevel)
			writeData(playerID .. ":SpawnArea:setupStep", 5)
		else
			CreatureObject(pPlayer):sendSystemMessage("You must enter a spawn level greater than 0.")
		end
    elseif (curStep == 5) then
		local radius = tonumber(args)

		if (radius ~= nil and radius > 0 and radius < 30) then
			writeData(playerID .. ":SpawnArea:radius", radius)
			writeData(playerID .. ":SpawnArea:setupCompleted", 1)
			self:spawnAreaEgg(pPlayer)
		else
			CreatureObject(pPlayer):sendSystemMessage("You must enter a spawn radius 0 to 30.")
		end
	elseif (curStep == 6) then --this is for re-doing the spawn
		if (args == "") then
			CreatureObject(pPlayer):sendSystemMessage("You must enter a spawn template.")
		else
			writeStringData(playerID .. ":SpawnArea:spawnTemplate", args)
			CreatureObject(pPlayer):sendSystemMessage("NPC Template: " .. args)
			writeData(playerID .. ":SpawnArea:setupStep", 7)
		end
	elseif (curStep == 7) then
		local spawnNumber = tonumber(args)

		if (spawnNumber ~= nil and spawnNumber > 0 and spawnNumber <= 20) then
			writeData(playerID .. ":SpawnArea:numSpawns", spawnNumber)
			writeData(playerID .. ":SpawnArea:setupStep", 8)
		else
			CreatureObject(pPlayer):sendSystemMessage("You must enter a spawn number between 1 and 20.")
		end
	elseif (curStep == 8) then
		local spawnLevel = tonumber(args)

		if (spawnLevel ~= nil and spawnLevel > 0) then
			writeData(playerID .. ":SpawnArea:spawnLevel", spawnLevel)
			writeData(playerID .. ":SpawnArea:setupStep", 9)
		else
			CreatureObject(pPlayer):sendSystemMessage("You must enter a spawn level greater than 0.")
		end
	elseif (curStep == 9) then
		local radius = tonumber(args)

		if (radius ~= nil and radius > 0 and radius < 30) then
			writeData(playerID .. ":SpawnArea:radius", radius)
			writeData(playerID .. ":SpawnArea:setupCompleted", 1)
			self:destroySpawnEgg(pPlayer)
			self:spawnAreaEgg(pPlayer)
		else
			CreatureObject(pPlayer):sendSystemMessage("You must enter a spawn radius 0 to 30.")
		end
	end

	self:showSetupUI(pPlayer)
end

--Menu option for the spawn egg.

SpawnEggMenuComponent = {}

function SpawnEggMenuComponent:fillObjectMenuResponse(pSceneObject, pMenuResponse, pPlayer)
	local menuResponse = LuaObjectMenuResponse(pMenuResponse)

	if (CreatureObject(pPlayer):hasSkill("admin_base")) then
		menuResponse:addRadialMenuItem(120, 3, "Info")
		menuResponse:addRadialMenuItem(121, 3, "Spawn")
		--menuResponse:addRadialMenuItemToRadialID(121, 50, 3, "Respawn Toggle")
		menuResponse:addRadialMenuItem(122, 3, "Remove")
		menuResponse:addRadialMenuItem(123, 3, "Modify")
	end
end

function SpawnEggMenuComponent:handleObjectMenuSelect(pObject, pPlayer, selectedID)
	if (pPlayer == nil or pObject == nil) then
		return 0
	end
	
	local pSpawnEgg = SceneObject(pObject):getObjectID()
	local pSpawnEggID = getSceneObject(pSpawnEgg)
	local posX = readData(pSpawnEgg .. ":SpawnArea:spawnPointX")
	local posZ = readData(pSpawnEgg .. ":SpawnArea:spawnPointZ")
	local posY = readData(pSpawnEgg .. ":SpawnArea:spawnPointY")
	local template = readStringData(pSpawnEgg .. ":SpawnArea:spawnTemplate")
	local mobLevel = readData(pSpawnEgg .. ":SpawnArea:spawnLevel")
	local numSpawns = readData(pSpawnEgg .. ":SpawnArea:numSpawns")
    local radius = readData(pSpawnEgg .. ":SpawnArea:radius")
	local heading = readData(pSpawnEgg .. ":SpawnArea:heading")
	local planet = readStringData(pSpawnEgg .. ":SpawnArea:spawnPlanet")
	local playerID = SceneObject(pPlayer):getObjectID()
	local toggle = readData(pSpawnEgg .. ":SpawnArea:toggle")

	if (selectedID == 120 and CreatureObject(pPlayer):hasSkill("admin_base")) then --Info
			--CreatureObject(pPlayer):sendSystemMessage("Spawn Egg ID: " .. pSpawnEgg .. ". X: " .. posX .. ". Z: " .. posZ .. ". Y: " .. posY .. ". Template: " .. template .. ". Level: " .. ". Number of Spawns: " .. numSpawns .. ". Radius: " .. radius .. ". Planet: " .. planet)
			CreatureObject(pPlayer):sendSystemMessage("Spawn Template: " .. template .. ". Radius: " .. radius .. ". Level: " .. mobLevel .. ". Number of Spawns: " .. numSpawns .. ".")
	end
	
	if (selectedID == 121 and CreatureObject(pPlayer):hasSkill("admin_base")) then --Spawn
		SpawnArea:spawnNpcsByMenu(pObject)
		CreatureObject(pPlayer):sendSystemMessage("Spawning from spawn egg.")
	end
	
	if (selectedID == 122 and CreatureObject(pPlayer):hasSkill("admin_base")) then --Remove
		writeData(pSpawnEgg .. ":SpawnArea:modifyOrRemove", 1)
		SceneObject(pSpawnEggID):destroyObjectFromWorld()
		CreatureObject(pPlayer):sendSystemMessage("Removing spawn egg.")
	end
	
	if (selectedID == 123 and CreatureObject(pPlayer):hasSkill("admin_base")) then --Modify
		writeData(pSpawnEgg .. ":SpawnArea:modifyOrRemove", 1)
		writeData(playerID .. ":SpawnArea:setupStep", 6)
		writeData(playerID .. ":SpawnArea:setupCompleted", 0)
		SpawnArea:showSetupUI(pPlayer)
		CreatureObject(pPlayer):sendSystemMessage("Sending spawn egg modification window.")
	end
	
	return 0
end