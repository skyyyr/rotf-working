AdminTools = {
	toolsMenu = {
		{ "Spawn Area", "openSpawnAreaConfig" },
		{ "Quest Tool", "openQuestToolConfig" },
	}
}

function AdminTools:openToolsSUI(pCreature)
	if (pCreature == nil) then
		return
	end

	self:openSUI(pCreature)
end

function AdminTools:openSUI(pCreature)
	local sui = SuiListBox.new("AdminTools", "mainSuiCallback")

	sui.setTargetNetworkId(SceneObject(pCreature):getObjectID())

	sui.setTitle("Admin Tools")
	sui.setPrompt("Select a tool below to open it.")

	for i = 1, #self.toolsMenu, 1 do
		sui.add(self.toolsMenu[i][1], "")
	end

	sui.sendTo(pCreature)
end

function AdminTools:mainSuiCallback(pPlayer, pSui, eventIndex, args)
	local cancelPressed = (eventIndex == 1)

	if (cancelPressed) then
		return
	end

	local chosenTool = args + 1

	self[self.toolsMenu[chosenTool][2]](pPlayer)
end

function AdminTools.openSpawnAreaConfig(pPlayer)
	SpawnArea:showMainUI(pPlayer)
end

function AdminTools.openQuestToolConfig(pPlayer)
	questTool:showMainUI(pPlayer)
end

function AdminTools:suiSpawnAreaCallback(pPlayer, pSui, eventIndex, args)
end

return AdminTools