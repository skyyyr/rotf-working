
--Made with Skyyyr's Wrench Tool 

leskConvoTemplate = ConvoTemplate:new {
	 initialScreen = "first",
	 templateType = "Lua",
	 luaClassHandler = "lesk_handler",
	 screens = {}
}

first = ConvoScreen:new {
	id = "first",
	leftDialog = "",
	customDialogText = "What do you want?",
	stopConversation = "false",
	options = {
		 {"Jam-gi sent me.","next"}
	}
 }
leskConvoTemplate:addScreen(first);

next = ConvoScreen:new {
	id = "next",
	leftDialog = "",
	customDialogText = "Ah, so you're another Brawler's Guild idiot trying to take my credits!",
	stopConversation = "false",
	options = {
		 {"Look just hand over the credits before someone gets hurt.","threat"},
		 {"I'm not looking for trouble Lesk.","no_trouble"}
	}
 }
leskConvoTemplate:addScreen(next);

threat = ConvoScreen:new {
	id = "threat",
	leftDialog = "",
	customDialogText = "You'll be the only one getting hurt. BOYS!!",
	stopConversation = "true",
	options = {
	}
 }
leskConvoTemplate:addScreen(threat);

no_trouble = ConvoScreen:new {
	id = "no_trouble",
	leftDialog = "",
	customDialogText = "Staying calm, what a good choice.. I'll give you the access number to the secure account. Just give it to Jam-gi.",
	stopConversation = "true",
	options = {
	}
 }
leskConvoTemplate:addScreen(no_trouble);

after_jump = ConvoScreen:new {
	id = "after_jump",
	leftDialog = "",
	customDialogText = "So you're tougher than you look.. I'm not looking to join them, here take this. Just give that to Jam-gi.. It's a secure line to my account.",
	stopConversation = "true",
	options = {
	}
 }
leskConvoTemplate:addScreen(after_jump);

not_ready = ConvoScreen:new {
	id = "not_ready",
	leftDialog = "",
	customDialogText = "I'm standing here minding my own business.",
	stopConversation = "true",
	options = {
	 }
 }
leskConvoTemplate:addScreen(not_ready);

completed = ConvoScreen:new {
	id = "completed",
	leftDialog = "",
	customDialogText = "I've already given you the credits! Sheesh!",
	stopConversation = "true",
	options = {
	 }
 }
leskConvoTemplate:addScreen(completed);

already = ConvoScreen:new {
	id = "already",
	leftDialog = "",
	customDialogText = "What do you want?",
	stopConversation = "true",
	options = {
	 }
 }
leskConvoTemplate:addScreen(already);


addConversationTemplate("leskConvoTemplate", leskConvoTemplate);