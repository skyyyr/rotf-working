
--Made with Skyyyr's Wrench Tool 

tutorialDroidConvoTemplate = ConvoTemplate:new {
	 initialScreen = "first",
	 templateType = "Lua",
	 luaClassHandler = "tutorial_droid_handler",
	 screens = {}
}

first = ConvoScreen:new {
	id = "first",
	leftDialog = "",
	customDialogText = "Hello master. I am pRotOcal T1-F2 at your service.",
	stopConversation = "false",
	options = {
		 {"Hello droid. Tell me about this place?","inquire_place"},
		 {"I'm looking to hone my skills","inquire_skill"},
		 {"That'll be all droid.","end_convo"}
	}
 }
tutorialDroidConvoTemplate:addScreen(first);

inquire_place = ConvoScreen:new {
	id = "inquire_place",
	leftDialog = "",
	customDialogText = "ERROR - The master hasn't given me anything to say to this response.",
	stopConversation = "true",
	options = {
	}
 }
tutorialDroidConvoTemplate:addScreen(inquire_place);

inquire_skill = ConvoScreen:new {
	id = "inquire_skill",
	leftDialog = "",
	customDialogText = "What type of skills are you looking to hone in on?",
	stopConversation = "false",
	options = {
		 {"I'd like to hone my skills as a brawler.","brawler_guild"}
	}
 }
tutorialDroidConvoTemplate:addScreen(inquire_skill);

end_convo = ConvoScreen:new {
	id = "end_convo",
	leftDialog = "",
	customDialogText = "Yes master.",
	stopConversation = "true",
	options = {
	}
 }
tutorialDroidConvoTemplate:addScreen(end_convo);

brawler_guild = ConvoScreen:new {
	id = "brawler_guild",
	leftDialog = "",
	customDialogText = "ERROR - The master hasn't given me anything to say to this response.",
	stopConversation = "true",
	options = {
	}
 }
tutorialDroidConvoTemplate:addScreen(brawler_guild);


addConversationTemplate("tutorialDroidConvoTemplate", tutorialDroidConvoTemplate);