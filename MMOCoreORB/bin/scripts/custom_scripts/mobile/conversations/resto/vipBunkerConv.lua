
--Made with Skyyyr's Wrench Tool 

vipBunkerConvoTemplate = ConvoTemplate:new {
	 initialScreen = "first",
	 templateType = "Lua",
	 luaClassHandler = "vip_bunker_handler",
	 screens = {}
}

first = ConvoScreen:new {
	id = "first",
	leftDialog = "",
	customDialogText = "Look, I'm busy working out details so be quick or get out.",
	stopConversation = "false",
	options = {
		 {"I was looking to buy a custom bunker.","bunker"},
		 {"What details are you working out?","details"},
		 {"I guess I'll go.","bye"}
	 }
 }
vipBunkerConvoTemplate:addScreen(first);

bye = ConvoScreen:new {
	id = "bye",
	leftDialog = "",
	customDialogText = "...",
	stopConversation = "true",
	options = {
	 }
 }
vipBunkerConvoTemplate:addScreen(bye);

details = ConvoScreen:new {
	id = "details",
	leftDialog = "",
	customDialogText = "What details? Am I just supposed to just include you on my sales deals? Let me guess you want some of the commission too?",
	stopConversation = "false",
	options = {
		 {"Commission? What?","question_commission"},
		 {"Maybe I could help for free?","free_help"}
	 }
 }
vipBunkerConvoTemplate:addScreen(details);

bunker = ConvoScreen:new {
	id = "bunker",
	leftDialog = "",
	customDialogText = "Well actually me too. I'm working some of my underground contacts for one. Maybe I could use someone like you.. Are you in?",
	stopConversation = "false",
	options = {
		 {"Sounds like you have a partner.","partner"},
		 {"Sounds shady, I'm out.","bye"}
	 }
 }
vipBunkerConvoTemplate:addScreen(bunker);

question_commission = ConvoScreen:new {
	id = "question_commission",
	leftDialog = "",
	customDialogText = ".... Hmm so you don't know... Alright, I'll offer you a deal! I have some underground contacts that I'm trying to get information from about this new bunker schematic. I was looking to get my hands on it.. You in?",
	stopConversation = "false",
	options = {
		 {"What's my cut?","my_cut"},
		 {"Sounds like a bad idea.","bye"}
	 }
 }
vipBunkerConvoTemplate:addScreen(question_commission);

free_help = ConvoScreen:new {
	id = "free_help",
	leftDialog = "",
	customDialogText = "FREE? HA! You must really think I'm stupid.. However, I could be convinced to let you in on this. You in?",
	stopConversation = "false",
	options = {
		 {"I'm in.","partner"},
		 {"I'll just leave you to it.","bye"}
	 }
 }
vipBunkerConvoTemplate:addScreen(free_help);

partner = ConvoScreen:new {
	id = "partner",
	leftDialog = "",
	customDialogText = "Great news partner! Alright, so here's the deal. I was about to finish a deal to obtain that schematic, but the deal fell through when the cantina got raided by some gang. I've been waiting on my contact to give me a location... but I haven't heard anything back in a while. How about you go see my contact and get the details. Here's his location. Come back with the details!",
	stopConversation = "true",
	options = {
	 }
 }
vipBunkerConvoTemplate:addScreen(partner);

my_cut = ConvoScreen:new {
	id = "my_cut",
	leftDialog = "",
	customDialogText = "I'll give you 100,000 credits. Are you in or not?",
	stopConversation = "false",
	options = {
		 {"Sounds fair to me.","partner"},
		 {"Right, that doesn't sound like enough for all the trouble.","bye"}
	 }
 }
vipBunkerConvoTemplate:addScreen(my_cut);

already = ConvoScreen:new {
	id = "already",
	leftDialog = "",
	customDialogText = "I told you to go see my contact. Get to it!",
	stopConversation = "true",
	options = {
	 }
 }
vipBunkerConvoTemplate:addScreen(already);

completed = ConvoScreen:new {
	id = "completed",
	leftDialog = "",
	customDialogText = "Did you get the details about the bunker?",
	stopConversation = "true",
	options = {
	 }
 }
vipBunkerConvoTemplate:addScreen(completed);


addConversationTemplate("vipBunkerConvoTemplate", vipBunkerConvoTemplate);