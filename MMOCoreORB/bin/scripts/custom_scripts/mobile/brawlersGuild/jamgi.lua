jamgi = Creature:new {
	objectName = "",
	socialGroup = "",
	customName = "Jam-gi (The Lone Fist)",
	faction = "",
	level = 100,
	chanceHit = 1,
	damageMin = 645,
	damageMax = 1000,
	baseXp = 9429,
	baseHAM = 24000,
	baseHAMmax = 30000,
	armor = 0,
	resists = {15,15,15,15,15,15,15,15,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_commoner_tatooine_rodian_female_02.iff",},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "jamgiConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(jamgi, "jamgi")