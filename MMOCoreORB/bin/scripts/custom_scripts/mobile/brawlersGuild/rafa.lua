rafa = Creature:new {
	objectName = "",
	socialGroup = "",
	customName = "Rafa Devia",
	faction = "",
	level = 100,
	chanceHit = 1,
	damageMin = 645,
	damageMax = 1000,
	baseXp = 9429,
	baseHAM = 24000,
	baseHAMmax = 30000,
	armor = 0,
	resists = {15,15,15,15,15,15,15,15,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/devaronian_male.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "rafaConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(rafa, "rafa")