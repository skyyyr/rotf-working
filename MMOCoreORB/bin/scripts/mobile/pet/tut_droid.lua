tut_droid = Creature:new {
	objectName = "",
    customName = "pRotOcal T1-F2 Droid",
	socialGroup = "",
	faction = "",
	level = 1,
	chanceHit = 1,
	damageMin = 1,
	damageMax = 2,
	baseXp = 1,
	baseHAM = 100,
	baseHAMmax = 110,
	armor = 0,
	resists = {0,0,0,0,0,0,0,0,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = NONE,
	creatureBitmask = NONE,
	optionsBitmask = INVULNERABLE + CONVERSABLE,
	diet = HERBIVORE,

	templates = {"object/mobile/3po_protocol_droid_red.iff"},
	lootGroups = {},
	weapons = {},
	conversationTemplate = "tutorialDroidConvoTemplate",
	attacks = {
	}
}

CreatureTemplates:addCreatureTemplate(tut_droid, "tut_droid")